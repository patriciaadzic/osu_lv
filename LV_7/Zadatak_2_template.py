import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

km = KMeans( n_clusters =5 , init ='k-means++', n_init =5) 
km.fit(img_array_aprox)
labels = km.predict(img_array_aprox)
quantizedColors = km.cluster_centers_[labels]
quantizedColors = np.reshape(quantizedColors, (w,h,d))

# prikaz rezultata
plt.figure()
plt.title("Rezultat")
plt.imshow(np.reshape(quantizedColors, (w,h,d)))
plt.tight_layout()
plt.show()

j = []
for n_clusters in range (1,10):
    km = KMeans( n_clusters =n_clusters , init ='k-means++', n_init =5) 
    km.fit(img_array_aprox)
    labels = km.predict(img_array_aprox)
    j.append(km.inertia_)

plt.plot(range(1,10),j, marker = 'o')
plt.xlabel('Broj klastera')
plt.ylabel('Inercija')
plt.tight_layout()
plt.show()

for i in range(0, 5):
    boolArray = labels == i
    img_array_aprox[boolArray] = km.cluster_centers_[i]
    plt.figure()
    plt.title("Binarna slika za klaster " + str(i))
    plt.imshow(boolArray.reshape((w,h)))
    plt.tight_layout()
    plt.show()

 