import numpy as np
import matplotlib . pyplot as plt

data = np.loadtxt("data.csv", delimiter=',', skiprows=1)

rows, cols = np.shape(data)
print(str(rows)," people were measured.")

height = data[:,1]
weight = data[:,2]

plt.scatter(height, weight, s=1)
 
plt.xlabel("Height [cm]")
plt.ylabel("Weight [kg]")
plt.title("Height to weight ratio")
plt.show()

height50 = height[::50]
weight50 = weight[::50]

plt.scatter(height50, weight50,s=5)
plt.xlabel("Height [cm]")
plt.ylabel("Weight [kg]")
plt.title("Height to weight ratio of every 50th person")
plt.show()

print("Min height: ", str(np.min(height)))
print("Max height: ", str(np.max(height)))
print("Average height: ", str(np.mean(height)))

men=data[np.where(data[:,0]==1)]
women=data[np.where(data[:,0]==0)]

print("Min male height: ", str(np.min(men[:,1])))
print("Max male height: ", str(np.max(men[:,1])))
print("Average male height: ", str(np.mean(men[:,1])))

print("Min female height: ", str(np.min(women[:,1])))
print("Max female height: ", str(np.max(women[:,1])))
print("Average female height: ", str(np.mean(women[:,1])))
#iris = datasets.load_iris()

#data = pd.DataFrame(data = iris.data, columns=iris.feature_names)

#data['target'] = iris.target