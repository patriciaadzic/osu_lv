numbers = []
while True:
    try:
        num_input = input("Unesite broj (ili 'Done' za kraj): ")
        if num_input.lower() == 'done':
            break
        num = float(num_input)
        numbers.append(num)
    except ValueError:
        print("Pogrešan unos! Molimo unesite broj.")
print("Unijeli ste", len(numbers), "brojeva.")
if numbers:
    print("Srednja vrijednost:", sum(numbers) / len(numbers))
    print("Minimalna vrijednost:", min(numbers))
    print("Maksimalna vrijednost:", max(numbers))
    numbers.sort()
    print("Sortirani brojevi:", numbers)