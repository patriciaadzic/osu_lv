def count_unique_words(file_path):
    word_count = {}
    with open(file_path, 'r') as file:
        for line in file:
            words = line.split()
            for word in words:
                word = word.lower()
                if word in word_count:
                    word_count[word] += 1
                else:
                    word_count[word] = 1
    
    unique_words = [word for word, count in word_count.items() if count == 1]
    print("Broj riječi koje se pojavljuju samo jednom:", len(unique_words))
    print("Te riječi su:", unique_words)

count_unique_words("song.txt")