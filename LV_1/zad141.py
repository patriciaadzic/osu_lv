def total_euro(hours, rate):
    return hours * rate

try:
    hours = float(input("Radni sati: "))
    rate = float(input("eura/h: "))
    total = total_euro(hours, rate)
    print("Ukupno:", total, "eura")
except ValueError:
    print("Pogrešan unos! Molimo unesite broj.")