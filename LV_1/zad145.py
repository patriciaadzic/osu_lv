def average_words_per_message(file_path, label):
    total_words = 0
    total_messages = 0
    with open(file_path, 'r') as file:
        for line in file:
            parts = line.split('\t')
            if parts[0] == label:
                total_words += len(parts[1].split())
                total_messages += 1
    if total_messages > 0:
        return total_words / total_messages
    else:
        return 0

def count_messages_with_exclamation(file_path, label):
    count = 0
    with open(file_path, 'r') as file:
        for line in file:
            parts = line.split('\t')
            if parts[0] == label and parts[1].endswith('!\n'):
                count += 1
    return count

average_words_ham = average_words_per_message("SMSSpamCollection.txt", "ham")
average_words_spam = average_words_per_message("SMSSpamCollection.txt", "spam")
print("Prosječan broj riječi u ham porukama:", average_words_ham)
print("Prosječan broj riječi u spam porukama:", average_words_spam)

exclamation_spam = count_messages_with_exclamation("SMSSpamCollection.txt", "spam")
print("Broj spam poruka koje završavaju uskličnikom:", exclamation_spam)
