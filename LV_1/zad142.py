try:
    grade = float(input("Unesite ocjenu (između 0.0 i 1.0): "))
    if grade < 0.0 or grade > 1.0:
        print("Ocjena mora biti između 0.0 i 1.0")
    elif grade >= 0.9:
        print("A")
    elif grade >= 0.8:
        print("B")
    elif grade >= 0.7:
        print("C")
    elif grade >= 0.6:
        print("D")
    else:
        print("F")
except ValueError:
    print("Pogrešan unos! Molimo unesite broj.")