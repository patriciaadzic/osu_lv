import numpy as np
from tensorflow import keras
from matplotlib import pyplot as plt
from PIL import Image

# Ponovno ucitajte model iz prethodnog zadatka
model = keras.models.load_model("mnist_model.h5")

# Ucitavanje slike test.png
image_path = "test.png"

# Ucitavanje slike i pretvaranje u crno-bijelu
image = Image.open(image_path).convert("L")

# Prikazi ucitanu sliku
plt.imshow(image, cmap="gray")
plt.axis("off")
plt.show()

# Prilagodavanje slike za ulaz u mrezu
image_resized = image.resize((28, 28))
image_array = np.array(image_resized)
image_array = image_array.astype("float32") / 255
image_array = np.expand_dims(image_array, axis=0)
image_array = np.expand_dims(image_array, axis=-1)

# Klasifikacija slike pomocu izgradene mreze
prediction = model.predict(image_array)
predicted_class = np.argmax(prediction)

# Ispis rezultata
print("Predicted digit:", predicted_class)
