import numpy as np
from tensorflow import keras
from matplotlib import pyplot as plt



model = keras.models.load_model("mnist_classifier.h5")

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

x_test_s = x_test.astype("float32") / 255
x_test_s = np.expand_dims(x_test_s, -1)

y_pred = model.predict(x_test_s)
y_pred_classes = np.argmax(y_pred, axis=1)

misclassified_indexes = np.where(y_pred_classes != y_test)[0]

num_images_to_display = 10

plt.figure(figsize=(12, 8))
for i, index in enumerate(misclassified_indexes[:num_images_to_display]):
    plt.subplot(1, num_images_to_display, i + 1)
    plt.imshow(x_test[index], cmap="gray")
    plt.title(f"True: {y_test[index]}, Predicted: {y_pred_classes[index]}")
    plt.axis("off")

plt.suptitle("Misclassified Images")
plt.show()
